import time
import random
import artlogo
print(artlogo.logo)
HANGMANPICS = ['''
  +---+
  |   |
      |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
  |   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
=========''']
def checkChar(ch1,word,hidden_word):
    index=-1
    newWordState = ""
    for ch in word:
        index+=1
        if(not hidden_word[index]=="_"):
            newWordState+=ch
            continue
        if(ch==ch1):
            newWordState+=ch
        else:
            newWordState+="_"
    return newWordState
word_list = ["elephant","cat","owl","mountain","road","apple","linux","linustorvalds","richardstallman","kernel","covid","bread","banana","engine","stevejobs","billgates","mouse","rat","water","dentist","boat","vulcano","tree","earth","piano","aluminium","socialmedia"]
chosen_word = random.choice(word_list)
hidden_word = ""
for ch in chosen_word:
  hidden_word+="_"
print(f"{hidden_word}")

hangman = 0;
while hangman<7:
    prev=hidden_word
    guess = input("guess a letter\n").lower()
    hidden_word=checkChar(guess,chosen_word,hidden_word)
    print(hidden_word)
    if(hidden_word.find("_")==-1):
        print('\x1b[0;39;43m' + 'U win' + '\x1b[0m')
        time.sleep( 3 )
        break
    else:
        if(prev==hidden_word):
            print(HANGMANPICS[hangman])
            hangman+=1
else:
    print('\x1b[0;39;43m' + 'U lose' + '\x1b[0m')
    time.sleep( 3 )
